import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled, { injectGlobal } from 'styled-components'
import { Login, Home, Cv } from './pages'
import { Route, withRouter } from 'react-router-dom'

const AppContainer = styled.section`
  max-width: 950px;
  margin: auto;
`

class App extends Component {
  componentDidMount() {
    injectGlobal`
      body {
        margin: 0;
        padding: 0;
        font-family: sans-serif;
        color: #222;
      }
      input {
        border-radius: 0.25rem;
        border: 1px solid #333;
        padding: 0.25rem;
        font-size: 1.2rem;
        outline-color: #000;
      }
    `

    if (this.props.mustRestoreProfile) {
      this.props.dispatch({ type: 'RESTORE_USERSESSION_REQUEST' })
    }
  }

  render() {
    if (this.props.profile) {
      return (
        <AppContainer>
          <Route exact path="/" component={Home} />
          <Route path="/cv" component={Cv} />
          <Route path="/home" component={Home} />
        </AppContainer>
      )
    } else {
      return (
        <AppContainer>
          <Route path="/" component={Login} />
        </AppContainer>
      )
    }
  }
}

App.propTypes = {
  profile: PropTypes.object,
  mustRestoreProfile: PropTypes.bool
}

const mapStateToProps = state => ({
  profile: state.global.get('profile'),
  mustRestoreProfile: state.global.get('mustRestoreProfile')
})

export default withRouter(connect(mapStateToProps)(App))

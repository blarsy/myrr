import PropTypes from 'prop-types'

const ExperiencePropType = {
  startDate: PropTypes.date,
  endDate: PropTypes.date,
  present: PropTypes.bool,
  role: PropTypes.string,
  employer: PropTypes.string,
  city: PropTypes.string,
  country: PropTypes.string,
  description: PropTypes.string
}

export { ExperiencePropType }

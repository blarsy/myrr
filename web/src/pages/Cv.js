import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  Block,
  LinkedInButton,
  ValidatedForm,
  getInitialState
} from '../toolbox'
import { ExperiencesList, TrainingsList, HobbiesList } from '../Components'

const inputs = [
  {
    type: 'text',
    name: 'title',
    required: true,
    label: 'Title'
  },
  {
    type: 'textArea',
    name: 'description',
    required: true,
    label: 'Description'
  }
]

class Cv extends React.Component {
  constructor(props) {
    super(props)

    this.state = getInitialState(inputs)
  }
  render() {
    return (
      <article>
        <Block align="center">
          <LinkedInButton
            onClick={() =>
              this.props.dispatch({ type: 'IMPORT_FROM_LINKEDIN' })}
          >
            Import from LinkedIn
          </LinkedInButton>
        </Block>
        <Block>
          <ValidatedForm
            title="General info"
            margin="25%"
            inputs={inputs}
            setState={state => this.setState(state)}
            getState={() => this.state}
            onSubmit={() =>
              this.props.dispatch({
                type: 'SAVE_CV_BASIC_DATA',
                data: this.state
              })}
            actionLabel="Save"
          />
          <ExperiencesList data={this.state.experiences || []} />
          <HobbiesList data={this.state.hobbies || []} />
          <TrainingsList data={this.state.trainings || []} />
        </Block>
      </article>
    )
  }
}

Cv.propTypes = {
  profile: PropTypes.object
}

const mapStateToProps = state => ({
  profile: state.global.get('profile')
})

export default connect(mapStateToProps)(Cv)

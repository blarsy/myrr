import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { Button } from '../toolbox'
import { push } from 'react-router-redux'

class Home extends React.Component {
  render() {
    return (
      <section>
        <p>Hi, {this.props.profile.firstName} {this.props.profile.lastName}</p>
        <Button onClick={() => this.props.dispatch({ type: 'LOGOUT_REQUEST' })}>
          Sign out
        </Button>
        <Button onClick={() => this.props.dispatch(push('cv'))}>
          Your CV
        </Button>
      </section>
    )
  }
}

Home.propTypes = {
  profile: PropTypes.object
}

const mapStateToProps = state => ({
  profile: state.global.get('profile')
})

export default connect(mapStateToProps)(Home)

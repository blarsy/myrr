import { Map } from 'immutable'
import auth from '../ext/Auth'

const initialState = {}
if (auth.isAuthenticated()) {
  initialState.mustRestoreProfile = true
}

export default (state = Map(initialState), action) => {
  switch (action.type) {
    case 'LOGIN_LINKEDIN_REQUEST':
      return state.set('loggingInLinkedIn', true)
    case 'LOGIN_SUCCESS':
      return state
        .set('loggingInLinkedIn', false)
        .set('profile', action.profile)
        .set('token', action.token)
    case 'LOGIN_LINKEDIN_ERROR':
      return state
        .set('loggingInLinkedIn', false)
        .set('loginError', action.error)
    case 'LOGIN_LINKEDIN_CANCELLED':
      return state.set('loggingInLinkedIn', false)
    case 'LOGOUT_SUCCESS':
      return state
        .set('loggingInLinkedIn', false)
        .set('profile', null)
        .set('token', null)
    case 'RESTORE_USERSESSION_SUCCESS':
      return state
        .set('profile', action.profile)
        .set('token', action.token)
        .set('mustRestoreProfile', false)
    default:
  }
  return state
}

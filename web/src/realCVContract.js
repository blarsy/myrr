const create = (
  profileId,
  trainings,
  experiences,
  hobbys,
  firstname,
  lastname,
  title,
  description
) => {}
const addTraining = (
  cvId,
  date,
  diploma,
  school,
  country,
  city,
  field,
  description,
  current
) => {}
const addExperience = (
  cvId,
  startDate,
  endDate,
  present,
  role,
  employer,
  city,
  country,
  description
) => {}
const addHobby = (cvId, startDate, endDate, name, description) => {}
const modifyTraining = (
  trainingId,
  date,
  diploma,
  school,
  country,
  city,
  field,
  description,
  current
) => {}
const modifyExperience = (
  experienceId,
  startDate,
  endDate,
  present,
  role,
  employer,
  city,
  country,
  description
) => {}
const modifyHobby = (hobbyId, startDate, endDate, name, description) => {}
const deleteExperience = experienceId => {}
const deleteTraining = trainingId => {}
const deleteHobby = hobbyId => {}
const certifyExperience = experienceId => {}
const certifyTraining = trainingId => {}
const certifyHobby = hobbyId => {}

export {
  create,
  addTraining,
  addExperience,
  addHobby,
  modifyTraining,
  modifyExperience,
  modifyHobby,
  deleteTraining,
  deleteExperience,
  deleteHobby,
  certifyTraining,
  certifyExperience,
  certifyHobby
}

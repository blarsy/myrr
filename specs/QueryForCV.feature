Feature:
In order to find suitable candidate for a job post
As a recruiter
I want to list CV's containing one or more keywords

Scenario: Query on a single keyword
Given CV's in the system
|Visible ?|Experience 1 title |Experience 1 description                   |Experience 2 title     |Experience 2 description       |Training 1                                       |Training 2 |Volunteering 1 title       |Volunteering 1 description                   |Volunteering 2 title             |Volunteering 2 description                                                                   |
|Yes      |Fireman trainee    |Trainership at the fire station of Kocity  |Professional fireman   |Putting out fire the cool way  |3 years evening course at Kocity's fire academy  |None       |Volunteer Fireman level 1  |Taking care mainly of logistics and support  |Volunteer fireman prevention dp  |Specialized in assisting the writing of security recommendation for new industrial buildings |
|No       |
When I search on keyword 'project lead'
Then Search results
|Relevance|Number of matches|Excerpts|
|

Scenario: Query using an AND operator

Scenario: Query using a NOT operator

Scenario: Query using an OR operator

Scenario: Query using all operators

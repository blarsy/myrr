'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.organisationType = exports.cityType = undefined;

var _graphql = require('graphql');

var cityType = exports.cityType = new _graphql.GraphQLObjectType({
  name: 'City',
  description: 'A city',
  fields: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: 'The id of the city.'
    },
    name: {
      type: _graphql.GraphQLString,
      description: 'The name of the city.'
    },
    countryCode: {
      type: _graphql.GraphQLString,
      description: 'The country code of the city.'
    }
  }
});

var organisationType = exports.organisationType = new _graphql.GraphQLObjectType({
  name: 'Organisation',
  description: 'An organisation',
  fields: {
    id: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLInt),
      description: 'The id of the organisation.'
    },
    name: {
      type: _graphql.GraphQLString,
      description: 'The name of the organisation.'
    }
  }
});
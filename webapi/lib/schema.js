'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphql = require('graphql');

var _graphqlSequelize = require('graphql-sequelize');

var _models = require('./models');

var _graphTypes = require('./graphTypes');

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var findLike = function findLike(findOptions, args) {
  findOptions.where = {
    name: _defineProperty({}, _sequelize2.default.Op.iLike, args.name + '%')
  };
  findOptions.order = [['name', 'ASC']];
  return findOptions;
};

var sortShorter = function sortShorter(results, args) {
  return results.sort(function (a, b) {
    var distA = a.name.length - args.name.length;
    var distB = b.name.length - args.name.length;
    if (distA < distB) {
      return -1;
    } else if (distA === distB) {
      return 0;
    } else {
      return 1;
    }
  });
};

exports.default = new _graphql.GraphQLSchema({
  query: new _graphql.GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      city: {
        type: new _graphql.GraphQLList(_graphTypes.cityType),
        args: {
          limit: { type: _graphql.GraphQLInt },
          name: { type: _graphql.GraphQLString }
        },
        resolve: (0, _graphqlSequelize.resolver)(_models.City, {
          before: findLike,
          after: sortShorter
        })
      },
      organisation: {
        type: new _graphql.GraphQLList(_graphTypes.organisationType),
        args: {
          limit: { type: _graphql.GraphQLInt },
          name: { type: _graphql.GraphQLString }
        },
        resolve: (0, _graphqlSequelize.resolver)(_models.Organisation, {
          before: findLike,
          after: sortShorter
        })
      }
    }
  })
});
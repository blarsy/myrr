'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

var _ramda2 = _interopRequireDefault(_ramda);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var citiesInArray = [];
_ramda2.default.forEachObjIndexed(function (value, key) {
  citiesInArray.push({
    id: key,
    lcaseName: value.name.toLowerCase(),
    name: value.name,
    countryCode: value.countryCode
  });
}, cities);

exports.default = {
  city: function city(args) {
    if (args.criteria.length > 2) {
      return _ramda2.default.sortBy(function (city) {
        return city.name.length;
      }, _ramda2.default.filter(function (city) {
        return city.lcaseName.startsWith(args.criteria.toLowerCase());
      }, citiesInArray));
    } else {
      return [];
    }
  }
};
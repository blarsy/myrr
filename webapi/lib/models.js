'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Organisation = exports.City = undefined;

var _config = require('./config');

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _graphqlSequelize = require('graphql-sequelize');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var sequelize = new _sequelize2.default('postgres://' + _config.DB_USER + ':' + _config.DB_PASSWORD + '@' + _config.DB_HOST + ':' + _config.DB_PORT + '/' + _config.DB_NAME);

var City = exports.City = sequelize.define('city', {
  id: { type: _sequelize2.default.INTEGER, primaryKey: true },
  name: _sequelize2.default.STRING,
  countryCode: _sequelize2.default.STRING
});

var Organisation = exports.Organisation = sequelize.define('organisation', {
  id: { type: _sequelize2.default.INTEGER, primaryKey: true },
  name: _sequelize2.default.STRING
});
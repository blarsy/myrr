import express from 'express'
import graphqlHTTP from 'express-graphql'
import cors from 'cors'

import { PORT, WEBSITE_URL, GRAPHQL_URL } from './config'
import schema from './schema'

const corsOptions = {
  origin: WEBSITE_URL,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

const app = express()
app.use(cors(corsOptions))

app.use(
  '/graphql',
  graphqlHTTP({
    schema,
    graphiql: true
  })
)

app.listen(PORT, () => console.log(`Now browse to ${GRAPHQL_URL}`))

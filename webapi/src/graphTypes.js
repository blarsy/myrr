import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLObjectType
} from 'graphql'

export const cityType = new GraphQLObjectType({
  name: 'City',
  description: 'A city',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'The id of the city.'
    },
    name: {
      type: GraphQLString,
      description: 'The name of the city.'
    },
    countryCode: {
      type: GraphQLString,
      description: 'The country code of the city.'
    }
  }
})

export const organisationType = new GraphQLObjectType({
  name: 'Organisation',
  description: 'An organisation',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'The id of the organisation.'
    },
    name: {
      type: GraphQLString,
      description: 'The name of the organisation.'
    }
  }
})

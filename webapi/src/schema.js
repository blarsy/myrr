import {
  graphql,
  GraphQLSchema,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList,
  GraphQLInt
} from 'graphql'
import { resolver } from 'graphql-sequelize'
import { City, Organisation } from './models'
import { cityType, organisationType } from './graphTypes'
import Sequelize from 'sequelize'

const findLike = (findOptions, args) => {
  findOptions.where = {
    name: { [Sequelize.Op.iLike]: `${args.name}%` }
  }
  findOptions.order = [['name', 'ASC']]
  return findOptions
}

const sortShorter = (results, args) => {
  return results.sort((a, b) => {
    const distA = a.name.length - args.name.length
    const distB = b.name.length - args.name.length
    if (distA < distB) {
      return -1
    } else if (distA === distB) {
      return 0
    } else {
      return 1
    }
  })
}

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
      city: {
        type: new GraphQLList(cityType),
        args: {
          limit: { type: GraphQLInt },
          name: { type: GraphQLString }
        },
        resolve: resolver(City, {
          before: findLike,
          after: sortShorter
        })
      },
      organisation: {
        type: new GraphQLList(organisationType),
        args: {
          limit: { type: GraphQLInt },
          name: { type: GraphQLString }
        },
        resolve: resolver(Organisation, {
          before: findLike,
          after: sortShorter
        })
      }
    }
  })
})

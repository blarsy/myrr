import { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME } from './config'
import Sequelize from 'sequelize'
import { resolver } from 'graphql-sequelize'

const sequelize = new Sequelize(
  `postgres://${DB_USER}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`
)

export const City = sequelize.define('city', {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  name: Sequelize.STRING,
  countryCode: Sequelize.STRING
})

export const Organisation = sequelize.define('organisation', {
  id: { type: Sequelize.INTEGER, primaryKey: true },
  name: Sequelize.STRING
})

CREATE USER myrcv WITH PASSWORD 'iZgzJdHQNvii5JUgWV58EzGGGXHP6eDaFKw4Y2ufY9msdJMWxrYoemnd7sx4Uehd';

REVOKE CONNECT ON DATABASE myrcv FROM PUBLIC;
GRANT CONNECT
ON DATABASE myrcv
TO myrcv;

REVOKE ALL
ON ALL TABLES IN SCHEMA public
FROM PUBLIC;

GRANT USAGE ON SCHEMA public to myrcv;

ALTER DEFAULT PRIVILEGES
IN SCHEMA public
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO myrcv;

ALTER DEFAULT PRIVILEGES
IN SCHEMA public
GRANT SELECT, USAGE ON SEQUENCES TO myrcv;

CREATE TABLE organisations (
    "id"          SERIAL PRIMARY KEY,
    "name"        varchar(200) NOT NULL,
    "createdAt"     timestamp DEFAULT CURRENT_TIMESTAMP,
    "updatedAt"     timestamp DEFAULT CURRENT_TIMESTAMP
);

COPY organisations (name) FROM '/docker-entrypoint-initdb.d/organisations.csv' WITH (FORMAT csv);

CREATE TABLE cities (
  "id" SERIAL PRIMARY KEY,
  "name" varchar(200) NOT NULL,
  "countryCode" varchar(3) NOT NULL,
  "createdAt"     timestamp DEFAULT CURRENT_TIMESTAMP,
  "updatedAt"     timestamp DEFAULT CURRENT_TIMESTAMP
);

COPY cities (name, "countryCode") FROM '/docker-entrypoint-initdb.d/cities5000.csv' WITH (FORMAT csv, delimiter E'\t');
